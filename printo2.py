#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 18:35:47 2018

@author: Tianyan
"""

from numpy	  import *
from numpy.random import *
from tssb		import *
from util		import *
from util2 import *

from ete2 import *

from subprocess import call
import sys
import os

import csv #need to write out to csv

ctr=0

#fout is now the file name
def print_top_trees(tree_archive,fout,outloc,k=2500):#edit to all 2500 trees
    global ctr;
    tree_reader = TreeReader(tree_archive)
    
    
    for idx, (tidx, llh, tree) in enumerate(tree_reader.load_trees_and_metadata(2500)):
        ctr=0
        remove_empty_nodes(tree.root, None)
        
#        print(idx)
        
        #current before matrix
        currBtMat = get_before_Matrix(tree)
        
        #current equal matrix
        currEqMat = get_equal_Matrix(tree)
        
        #first time is just the unchanged matrix
        if idx == 0:
            before_result = add_blank_Matrix(currBtMat, 0)
            equal_result = add_blank_Matrix(currEqMat, 0)
            gdict = get_gene_dict(tree)
        #rest of the times add the current tree to the existing
        else:
            before_result = add_Matrix(currBtMat, before_result)
            equal_result = add_Matrix(currEqMat, equal_result)
    
    after_result = transpose_Matrix(before_result)
    
    before_result_csv = add_matrix_row_col_names(before_result, gdict)
    after_result_csv = add_matrix_row_col_names(after_result, gdict)
    equal_result_csv = add_matrix_row_col_names(equal_result, gdict)

    #write after matrix results to csv
    #use [:-4] to get rid of .txt extension in fout
    #use basename to prevent fout from containing the input directory
    with open(os.path.join(outloc,os.path.basename(fout[:-4])+"_after.csv"), 'w') as afterFile:
        writer = csv.writer(afterFile)
        writer.writerows(after_result_csv)
        
    #write before matrix results to csv
    with open(os.path.join(outloc,os.path.basename(fout[:-4])+"_before.csv"), 'w') as beforeFile:
        writer = csv.writer(beforeFile)
        writer.writerows(before_result_csv)
    
    #write equal matrix results to csv
    with open(os.path.join(outloc,os.path.basename(fout[:-4])+"_equal.csv"), 'w') as equalFile:
        writer = csv.writer(equalFile)
        writer.writerows(equal_result_csv)
    
    tree_reader.close()

def get_before_Matrix(tssb):
	
    t = Tree();t.name='0'
    
    #total number of genes in the tree
    total_genes = get_num_genes(tssb.root, None, t)
    
    #matrix to hold the before pairs
    before_count_Matrix = [[0 for x in range(total_genes)] for y in range(total_genes)]
    
    #fill the matrix
    fill_before_Matrix(tssb.root, None, t, before_count_Matrix)
        
#    print(before_count_Matrix)
    
    return before_count_Matrix

def get_equal_Matrix(tssb):
    
    t = Tree();t.name='0'
    
    #total number of genes in the tree
    total_genes = get_num_genes(tssb.root, None, t)
    
    #matrix to hold the equal pairs
    equal_count_Matrix = [[0 for x in range(total_genes)] for y in range(total_genes)]
    
    #fill the matrix
    fill_equal_Matrix(tssb.root, None, t, equal_count_Matrix)
    
#    print(equal_count_Matrix)
    
    return equal_count_Matrix

#before than matrix
#note that this function does not return anything, it fills in btMat
#note that this matrix wouldn't include non-descendant counts
#i.e. if the structure were
#   
#     /---1---3
#   0
#     \---2---4
# the genes in 4 would not be included in the descendants of 1 
def fill_before_Matrix(node, parent,tree, btMat):
    global ctr;
    node_name  = ctr ; ctr+=1;
    
    #genes in the node
    genes = node['node'].get_data()
    
    #descendant genes
    desc = get_gene_descendants(node, parent, tree)
    
    #loop through the descendants and change the names to numbers
    for ids in range(len(desc)):
        desc[ids] = convert_gene_name_to_int(desc[ids])
        
    
    #change names to numbers
    for g in range(len(genes)):
        genes[g].id = convert_gene_name_to_int(genes[g].id)
    
    #need to have genes to work with
    if(len(genes))>0:
        #loop through the genes 
        for i in range(len(genes)):
            #loop through the descendants
            for d in range(len(desc)):
                #compare each of this level's genes to all lower level
                btMat[genes[i].id][desc[d]] = 1
            
    #recursion
    for child in node['children']:
		name_string = str(ctr)#+'('+str(len(child['node'].get_data()))+')'
		fill_before_Matrix(child, node_name,tree.add_child(name=name_string), btMat)
    
##before than matrix
##note that this function does not return anything, it fills in btMat
##note that this version tries to include non-descendant counts
##i.e. if the structure were
##   
##     /---1---3
##   0
##     \---2---4
## the genes in 4 would be included in the descendants of 1 
#def fill_before_Matrix2(node, parent, tree, btMat):
#    global ctr;
#    node_name  = ctr ; ctr+=1;
#    
#    #genes in the node
#    genes = node['node'].get_data()
#    
#    #descendant genes
#    desc = get_gene_descendants2(node, parent, tree)
#    
#    #loop through the descendants and change the names to numbers
#    for ids in range(len(desc)):
#        desc[ids] = convert_gene_name_to_int(desc[ids])
#
#    #change names to numbers
#    for g in range(len(genes)):
#        genes[g].id = convert_gene_name_to_int(genes[g].id)
#    
#    #need to have genes to work with
#    if(len(genes))>0:
#        #loop through the genes 
#        for i in range(len(genes)):
#            #loop through the descendants
#            for d in range(len(desc)):
#                #compare each of this level's genes to all lower level
#                btMat[genes[i].id][desc[d]] = 1
#            
#    #recursion
#    for child in node['children']:
#		name_string = str(ctr)#+'('+str(len(child['node'].get_data()))+')'
#		fill_before_Matrix2(child, node,tree.add_child(name=name_string), btMat)
    

#equal level matrix
#i.e. if the structure were
#   
#     /---1---3
#   0
#     \---2---4
# the genes in 1 one would only be equal to the others within it (not 2)
def fill_equal_Matrix(node, parent, tree, eqMat):
    global ctr;
    node_name  = ctr ; ctr+=1;
    
    #genes in the node
    genes = node['node'].get_data()
    
    #change names to numbers
    for g in range(len(genes)):
        genes[g].id = convert_gene_name_to_int(genes[g].id)
    
    
    #always end up going through the node internally
    #need genes to work with
    if(len(genes))>0:
        #loop through genes twice
        for i in range(len(genes)):
            for j in range(len(genes)):
                #each pair within is equal level
                #make sure that both i,j and j,i are set to 1
                eqMat[genes[i].id][genes[j].id] = 1
                eqMat[genes[j].id][genes[i].id] = 1
    
#    #there is a parent
#    if parent != None:    
#        #loop through the children of this node's parent
#        #i.e. the siblings and the node itself
#        for child in parent['children']:
#            #already considered internal
#            if child != node:
#                #genes in the child
#                cgenes = child['node'].get_data()
#                
#                #change names to numbers
#                for cg in range(len(cgenes)):
#                    cgenes[cg].id = convert_gene_name_to_int(cgenes[cg].id)
#                
#                #need to have genes to work with
#                if len(cgenes)>0 and len(genes)>0:
#                    #loop through genes and cgenes
#                    for gi in range(len(genes)):
#                        for cgi in range(len(cgenes)):
#                            #each pair within is equal level
#                            #make sure that both gi,cgi and cgi,gi are set to 1
#                            eqMat[genes[gi].id][cgenes[cgi].id] = 1
#                            eqMat[cgenes[cgi].id][genes[gi].id] = 1
    

    #recursion
    for child in node['children']:
        name_string = str(ctr)
        fill_equal_Matrix(child, node,tree.add_child(name=name_string), eqMat)
    

############## miscellaneous helper functions ################################


#get the total number of genes in the tree
def get_num_genes(node, parent, tree):
    #all data
    genes = node['node'].get_data()
    
    #number of genes in this node
    num_genes = 0
    num_genes += len(genes)
    
    #recursively go through the tree
    for child in node['children']:
		num_genes += get_num_genes(child, node, tree)
    
    #after all recursions, should be total number
    return num_genes

#has one parameter, calls the helper
def get_gene_dict(tssb):
    t = Tree();t.name='0'
    return get_gene_dict_helper(tssb.root, None, t)

#create a dictionary to map from the gene id to the name
def get_gene_dict_helper(node, parent, tree):
    #gene dictionary
    gene_dict = {}
    
    #all data
    genes = node['node'].get_data()
    
    #need genes to work with
    if len(genes) > 0:
        #for each gene
        for g in arange(0, len(genes)):
            #key is the gene id number
            gnum = convert_gene_name_to_int(genes[g].id)
            #value is the name of the gene
            gene_dict[gnum] = genes[g].name
    
    #recursively go through the tree
    for child in node['children']:
        #use update to continuously edit the original dictionary
		gene_dict.update(get_gene_dict_helper(child, node, tree))
    
    #after all recursions, should be all genes in the dictionary
    return gene_dict

#get a list of the genes below a node
#direct descendants only
def get_gene_descendants(node, parent, tree):
    #output list
    out = []
    
    #recursively go through the tree
    for child in node['children']:
        #genes of the child
        genes = child['node'].get_data()
        
        #has genes
        if len(genes)>0:
            #for each gene, add it to out
            for g in arange(0,len(genes)):
                out.append(genes[g].id)
                
        #use extend to prevent lists in lists    
        out.extend(get_gene_descendants(child, node, tree))
    
    return remove_duplicates(out)

##get a list of the genes below a node
##not necessarily direct descendants
## still a slight problem if the structure were
##   
##              /---5
##     /---1---3
##    /         \---6
##   0          
##    \      
##     \---2---4
## the genes in 5 and 6 would not be listed as descendants of 4
#def get_gene_descendants2(node, parent, tree):
#    #output list
#    out = []
#        
#    #recursion to get all of node's direct descendants
#    for child in node['children']:
#        #use extend to prevent lists in lists    
#        out.extend(get_gene_descendants(child, node, tree))
#    
#    if parent!= None:
#        #recursion to get all of node's indirect descendants
#        #look at the sibling's direct descendants
#        for sibling in parent['children']:
#            out.extend(get_gene_descendants(sibling, parent, tree))
#        
#    
#    return remove_duplicates(out)

#convert a string to int
#if a isn't already int, it is of the form s10
def convert_gene_name_to_int(a):
    if type(a) == int:
        return a
    else:
        return int(a[1:])
        

 

#note that X, Y are of same size
def add_Matrix(X, Y):
    #resultant matrix
    resultant = [[0 for x in range(len(X))] for y in range(len(X[0]))]
    
    # iterate through rows
    for i in range(len(X)):
        # iterate through columns
        for j in range(len(X[0])):
            resultant[i][j] = X[i][j] + Y[i][j]
    
    return resultant

#take in an int blank and add it to a matrix
def add_blank_Matrix(X, blank):
    #create a matrix of same size as X
    blank_Matrix = [[blank for x in range(len(X))] for y in range(len(X[0]))]
    #add the two matrices
    return add_Matrix(X, blank_Matrix)

#transpose a matrix (switch the rows and columns)
def transpose_Matrix(X):
    trans = [[X[j][i] for j in range(len(X))] for i in range(len(X[0]))]
    return trans

#adds row and col names to the output matrix using the gene dictionary
def add_matrix_row_col_names(X, d):
    gnames = d.values()
    
    new_row_len = len(X) + 1
    new_col_len = len(X[0]) + 1
    
    newMat = [[0 for x in range(new_row_len)] for y in range(new_col_len)]
    
    for i in range(len(newMat)):
        for j in range(len(newMat[0])):
            if i == 0 and j == 0:
                newMat[i][j] = ''
            elif i == 0 and j != 0:
                newMat[i][j] = gnames[j-1]
            elif j == 0 and i != 0:
                newMat[i][j] = gnames[i-1]
            else:
                newMat[i][j] = X[i-1][j-1]
    
    return newMat

#write matrix to text
#f should already be opened
def write_matrix_to_textfile(a_matrix, f):

    def compile_row_string(a_row):
        return str(a_row).strip(']').strip('[').replace(' ','')

    
    for row in a_matrix:
        f.write(compile_row_string(row)+'\n')

    return True

def remove_duplicates(l):
    return list(set(l))